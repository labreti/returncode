package returncode;

public class ReturnCode {
	public static void main(String[] args) throws InterruptedException {
		RetCode e = new RetCode(0);
		Thread t=null;
		(t = new Test(e)).start();
		t.join();
		System.out.println(e.get());
	}
	static class Test extends Thread {
		RetCode r=null;
		public Test(RetCode r) { this.r=r; }
		public void run() { r.set(14); }
	}
	static class RetCode {
		private int x;
		public RetCode(int i) { x=i; }
		public void set(int i) { x=i; }
		public int get() { return x; }
	}
}
